This is an implementation of the
[peaksea](https://www.vim.org/scripts/script.php?script_id=760) colour theme from [vim ](https://github.com/vim/vim) for [vis](https://github.com/martanne/vis).

# Set the theme

To set the theme of vis you have to add:

	vis.events.subscribe(vis.events.INIT, function()
		background="dark" --color of the theme(dark|light)
		vis:command('set theme peaksea/peaksea') --path to the theme
	end)

in your visrc.lua
